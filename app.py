import models.bidworkflow.bid.bidaggregator as report_gen
from tkinter import filedialog
from tkinter import messagebox
from tkinter import *
from os import startfile

root = Tk()

root.title("Fap App")

# Add a grid
mainframe = Frame(root)
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)
mainframe.pack(pady=100, padx=100)

tkvar = StringVar(root)

# Dictionary with options
choices = {'Lot 5 Sitework', 'Trade 1', 'Trade 2', 'Trade 3', 'Quick Fap'}
tkvar.set('Lot 5 Sitework')  # set the default option

popupMenu = OptionMenu(mainframe, tkvar, *choices)
Label(mainframe, text="Select a trade to compile report for").grid(row=1, column=1)
popupMenu.grid(row=2, column=1)


# on change dropdown value
def change_dropdown(*args):

    root.withdraw()
    model_bid_file = filedialog.askopenfile(title="Select file to model pricing against")
    input_dir_selected = filedialog.askdirectory(title="Select folder containing model files")
    output_dir_selected = filedialog.askdirectory(title="Select location to write report")

    report_path = report_gen.create_bid_report(model_bid_file.name,
                                               input_dir_selected,
                                               output_dir_selected, tkvar.get())
    open_report(report_path)


def open_report(report_path):
    result = messagebox.askquestion("Fap App", "Your file was saved to {} would you like to open it?"
                                    .format(report_path))

    if result == 'yes':
        startfile(report_path)


tkvar.trace('w', change_dropdown)

root.mainloop()
