from openpyxl.styles import Font
from openpyxl.styles import PatternFill


def print_bid_meta(bid, report_ws, start_row, start_col):
    report_ws.cell(row=start_row, column=start_col).value = "Subcontractor Name"
    report_ws.cell(row=start_row, column=start_col).font = Font(bold=True)
    report_ws.cell(row=start_row + 1, column=start_col).value = bid.bidder

    report_ws.cell(row=start_row, column=start_col + 1).value = "Email"
    report_ws.cell(row=start_row, column=start_col + 1).font = Font(bold=True)
    report_ws.cell(row=start_row + 1, column=start_col + 1).value = bid.email

    report_ws.cell(row=start_row, column=start_col + 2).value = "Phone"
    report_ws.cell(row=start_row, column=start_col + 2).font = Font(bold=True)
    report_ws.cell(row=start_row + 1, column=start_col + 2).value = bid.phone

    report_ws.cell(row=start_row, column=start_col + 3).value = "Date"
    report_ws.cell(row=start_row, column=start_col + 3).font = Font(bold=True)
    report_ws.cell(row=start_row + 1, column=start_col + 3).value = bid.date


def print_bid_components(model_bid, bid_components, report_ws, start_row, start_col):
    curr_row = start_row
    curr_col = start_col

    report_ws.cell(row=curr_row, column=curr_col).value = "Component"
    report_ws.cell(row=curr_row, column=curr_col).font = Font(bold=True)

    report_ws.cell(row=curr_row, column=curr_col + 1).value = "Description"
    report_ws.cell(row=curr_row, column=curr_col + 1).font = Font(bold=True)

    report_ws.cell(row=curr_row, column=curr_col + 2).value = "Quantity"
    report_ws.cell(row=curr_row, column=curr_col + 2).font = Font(bold=True)

    report_ws.cell(row=curr_row, column=curr_col + 3).value = "Unit Cost"
    report_ws.cell(row=curr_row, column=curr_col + 3).font = Font(bold=True)

    report_ws.cell(row=curr_row, column=curr_col + 4).value = "Total Cost"
    report_ws.cell(row=curr_row, column=curr_col + 4).font = Font(bold=True)

    report_ws.cell(row=curr_row, column=curr_col + 5).value = "Adjustments"
    report_ws.cell(row=curr_row, column=curr_col + 5).font = Font(bold=True)

    if model_bid is not None:
        report_ws.cell(row=curr_row, column=curr_col + 6).value = "Delta"
        report_ws.cell(row=curr_row, column=curr_col + 6).font = Font(bold=True)

    curr_row += 1

    for key, component in bid_components.items():
        report_ws.cell(row=curr_row, column=curr_col).value = component.component

        report_ws.cell(row=curr_row, column=curr_col + 1).value = component.description

        report_ws.cell(row=curr_row, column=curr_col + 2).value = component.quantity

        report_ws.cell(row=curr_row, column=curr_col + 3).value = component.unitcost
        report_ws.cell(row=curr_row, column=curr_col + 3).number_format = '#,##0.00$'

        report_ws.cell(row=curr_row, column=curr_col + 4).value = component.unitcost * component.quantity
        report_ws.cell(row=curr_row, column=curr_col + 4).number_format = '#,##0.00$'

        report_ws.cell(row=curr_row, column=curr_col + 5).value = component.adjustments
        report_ws.cell(row=curr_row, column=curr_col + 5).number_format = '#,##0.00$'

        if model_bid is not None:
            model_component = model_bid.bidcomponents[component.key()]
            delta = (component.unitcost * component.quantity) - (model_component.unitcost * model_component.quantity)
            report_ws.cell(row=curr_row, column=curr_col + 6).value = delta
            if delta > 0:
                report_ws.cell(row=curr_row, column=curr_col + 6).fill = PatternFill("solid", fgColor="00FF00")
            elif delta < 0:
                report_ws.cell(row=curr_row, column=curr_col + 6).fill = PatternFill("solid", fgColor="FF0000")
            report_ws.cell(row=curr_row, column=curr_col + 6).number_format = '#,##0.00$'

        curr_row += 1


def print_bid(model_bid, report_ws, bid, start_row, start_col):
    print_bid_meta(bid, report_ws, start_row, start_col)
    print_bid_components(model_bid, bid.bidcomponents, report_ws, start_row + 3, start_col)


def adjust_ws_rows(report_ws):
    dims = {}
    for row in report_ws.rows:
        for cell in row:
            if cell.value:
                dims[cell.column] = max((dims.get(cell.column, 0), len(str(cell.value))))
    for col, value in dims.items():
        report_ws.column_dimensions[col].width = value + 1
