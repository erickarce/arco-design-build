class EstimateItem:
    def __init__(self, trade, component, subcomponent, description, qty, unit,
                 unit_labor, unit_matls, unit_subcon,
                 total_labor, total_matls, total_subcon,
                 total):
        self.trade = trade
        self.component = component
        self.subcomponent = subcomponent
        self.description = description
        self.qty = qty
        self.unit = unit
        self.unit_labor = unit_labor
        self.unit_matls = unit_matls
        self.unit_subcon = unit_subcon
        self.total_labor = total_labor
        self.total_matls = total_matls
        self.total_subcon = total_subcon
        self.total = total
