class BidComponent:
    def __init__(self, component, description, quantity, unitcost, adjustments):
        self.component = component
        self.description = description
        self.quantity = quantity
        self.unitcost = unitcost
        self.adjustments = adjustments
        self.cell = None

    def is_empty_row(self):
        if self.component is None \
                and self.description is None \
                and self.quantity is None \
                and self.unitcost is None \
                and self.adjustments is None:
            return True
        else:
            return False

    def set_location(self, cell):
        self.cell = cell

    def key(self):
        return self.component + "_" + self.description
