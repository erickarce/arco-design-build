import os
import models.bidworkflow.bid as bidd
import models.bidworkflow.bid.bidcomponent as bidcomp
import models.bidworkflow.validator as validator
import models.bidworkflow.templateprinter as templator
from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.styles import PatternFill
from datetime import date


def create_bid_report(model_bid_file, bid_input_dir, report_output_dir, report_name):
    model_bid = load_bid(load_workbook(model_bid_file).active)
    bids = load_workbooks(bid_input_dir)
    if not validator.integrity(model_bid, bids):
        return

    report_wb = gen_simple_report(report_name)
    report_ws = report_wb.active

    report_ws.cell(row=3, column=2).value = "More Cost Then Model Proposal"
    report_ws.cell(row=3, column=3).fill = PatternFill("solid", fgColor="00FF00")
    report_ws.cell(row=4, column=2).value = "Less Cost Then Model Proposal"
    report_ws.cell(row=4, column=3).fill = PatternFill("solid", fgColor="FF0000")

    curr_row = 11
    curr_col = 1
    templator.print_bid(None, report_ws, model_bid, curr_row, curr_col)
    curr_col += 7

    for bid in bids:
        templator.print_bid(model_bid, report_ws, bid, curr_row, curr_col)
        curr_col += 8

    templator.adjust_ws_rows(report_ws)

    report_path = report_output_dir + "\\" + report_name + " " + str(date.today().strftime('%Y-%m-%d')) + ".xlsx"
    report_wb.save(report_path)
    return report_path


def load_workbooks(basedir):
    files = [f for f in os.listdir(basedir) if os.path.isfile(os.path.join(basedir, f))]
    bids = []
    for file in files:
        wb = load_workbook(basedir + "\\" + file)
        sheet = wb.active
        print("loaded [{}] from workbook [{}]".format(wb.active.title, file))
        bids.append(load_bid(sheet))
    return bids


def load_bid(sheet):
    phone = sheet['C2'].internal_value
    phone = phone if isinstance(phone, str) else str(int(phone))
    phone = phone.replace("-", "").replace(".", "")

    bid = bidd.Bid(
        sheet['A2'].internal_value,
        sheet['B2'].internal_value,
        phone,
        sheet['D2'].internal_value)

    for row in sheet.iter_rows(min_row=5, max_col=6):
        component = bidcomp.BidComponent(
            row[0].internal_value,
            row[1].internal_value,
            row[2].internal_value,
            row[3].internal_value,
            row[5].internal_value)
        if not component.is_empty_row():
            bid.add_bidcomponent(component)
    print("[{}] components loaded for [{}]".format(len(bid.bidcomponents), sheet['A2'].internal_value))
    return bid


def gen_simple_report(bid):
    simple_wb = Workbook()
    simple_ws = simple_wb.active
    simple_ws.title = bid + " Bid " + str(date.today().strftime('%Y-%m-%d'))
    return simple_wb


