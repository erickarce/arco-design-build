class Bid:
    def __init__(self, bidder, email, phone, date):
        self.bidder = bidder
        self.email = email
        self.phone = phone
        self.date = date
        self.bidcomponents = {}

    def add_bidcomponent(self, component):
        self.bidcomponents[component.key()] = component
